import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateCategoryComponent } from './category/create-category/create-category.component';
import { EditCategoryComponent } from './category/edit-category/edit-category.component';
import { ListCategoryComponent } from './category/list-category/list-category.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  { path : "", component : HomeComponent},
  { path : "login", component : LoginComponent},
  { path : "register", component : RegisterComponent },
  { path : "category", component : ListCategoryComponent}, 
  { path : "add-category", component : CreateCategoryComponent },
  { path : "edit-category/:id", component : EditCategoryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
