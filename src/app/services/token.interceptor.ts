import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let token : string | null = localStorage.getItem('token');
    //Est-ce qu'on a un token stockée en localStorage ?
    if(token){
      //Et si notre token n'est pas une chaine vide
      if(token != '') {
         //Si oui, on l'ajoute dans les headers de la requête
        let clonedRequest = request.clone({ setHeaders : { 'Authorization' : 'Bearer ' + token }})
        return next.handle(clonedRequest); 
        //Pensez à ajouter dans les providers de votre module, votre interceptor
      }
     
    }
    return next.handle(request);
  }
}
