import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from '../models/category-model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private _url : string = 'http://localhost:8080/api/category/';

  constructor(private _httpClient : HttpClient) { }

  getAll() : Observable<Category[]>{
    return this._httpClient.get<Category[]>(this._url);
  }

  getById(id : string) : Observable<Category> {
    return this._httpClient.get<Category>(this._url+id);
  }

  create(cat : Category){
    return this._httpClient.post(this._url, cat);
  }

  update(cat : Category, id : string){
    return this._httpClient.put(this._url+id, cat);
  }

  delete(id : string){
    return this._httpClient.delete(this._url+id);
  }
}
