import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { Login } from '../models/login-model';
import { Register } from '../models/register-model';
import { Token } from '../models/token-model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  //A remplacer par votre API
  private _url : string = "http://localhost:8080/api/auth/";

  //Observable connection
  private _isConnected$ : BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isConnected$ : Observable<boolean> = this._isConnected$.asObservable();

  constructor(private _httpClient : HttpClient, private _router : Router) { }

  // register(registerForm : Register) : Observable<Token>{
  //   return this._httpClient.post<Token>(this._url+'register', registerForm);
  // }

  register(registerForm : Register) : void {
    this._httpClient.post<Token>(this._url+'register', registerForm).subscribe({
      next : (res) => {
        //On met le token en localStorage
        localStorage.setItem('token', res.token);
        this._isConnected$.next(true);
        this._router.navigateByUrl('/');

      }
    });
  }

  // login(loginForm : Login) : Observable<Token> {
  //   return this._httpClient.post<Token>(this._url+'login', loginForm);
  // }

  login(loginForm : Login) : void {
    this._httpClient.post<Token>(this._url+'login', loginForm).subscribe({
      next : (res) => {
        localStorage.setItem('token', res.token);
        this._isConnected$.next(true);
        this._router.navigateByUrl('/');
      }
    }
    );
  }

  logout() : void {
    localStorage.removeItem('token');
    this._isConnected$.next(false);
    this._router.navigateByUrl('/');

  }
}
