import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isConnected : boolean = false;

  constructor(private _authService : AuthService) {  }

  ngOnInit(): void {
    this._authService.isConnected$.subscribe({
      next : (state) => {
        this.isConnected = state;
      }
    })
  }

  logout() : void {
    this._authService.logout();
  }

}
