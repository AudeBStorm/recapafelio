export class Register {
    pseudo : string;
    email : string;
    firstname : string;
    lastname : string;
    password : string;

    constructor(pseudo : string, email : string, first : string, last : string, pwd : string){
        this.pseudo = pseudo;
        this.email = email;
        this.firstname = first;
        this.lastname = last;
        this.password = pwd;
    }
}