export class Category {
    _id : string ;
    name : string;
    icon : string;

    constructor(id : string, name : string, icon : string) {
        this._id = id;
        this.name = name;
        this.icon = icon;
    }
}