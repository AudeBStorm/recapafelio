import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category-model';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-list-category',
  templateUrl: './list-category.component.html',
  styleUrls: ['./list-category.component.scss']
})
export class ListCategoryComponent implements OnInit {

  categories : Category[] = [];

  constructor(private _categoryService : CategoryService) { }

  ngOnInit(): void {
    this._categoryService.getAll().subscribe({
      next : (res) => {
        this.categories = res;
      }
    })
  }

  delete(id : string){
    this._categoryService.delete(id).subscribe({
      next : () => {
        this._categoryService.getAll().subscribe({
          next : (res) => {
            this.categories = res;
          }
        })
      }
    })
  }

}
