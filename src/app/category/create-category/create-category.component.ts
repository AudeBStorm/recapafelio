import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Category } from 'src/app/models/category-model';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.scss']
})
export class CreateCategoryComponent implements OnInit {

  categoryForm : FormGroup;

  constructor(private _fb : FormBuilder, private _categoryService : CategoryService, 
            private _router : Router) { 
    this.categoryForm = this._fb.group({
      name : [null, [Validators.required]],
      icon : [null, [Validators.required]]
    })
  }

  ngOnInit(): void {
  }

  createCat(){
    if(this.categoryForm.valid) {
      //Si formulaire valide, on appelle le post de notre service
      let catForm : Category = this.categoryForm.value;
      this._categoryService.create(catForm).subscribe({
        next : () => {
          this._router.navigateByUrl("/category");
        }
      })
    }
    else {
      this.categoryForm.markAllAsTouched();
    }
  }

}
