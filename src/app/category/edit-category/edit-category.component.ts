import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/models/category-model';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {
  
  categoryForm : FormGroup;
  categoryId! : string;
  category! : Category;


  constructor(private _fb : FormBuilder, private _activeRoute : ActivatedRoute, 
            private _categoryService : CategoryService, private _router : Router) { 
    this.categoryForm = this._fb.group({
      name : [null, [Validators.required]],
      icon : [null, [Validators.required]]
    })
  }

  ngOnInit(): void {
    //On récupère l'id
    this.categoryId = this._activeRoute.snapshot.params['id'];
    this._categoryService.getById(this.categoryId).subscribe({
      next : (res) => {
        //res contient la catégorie trouvée
        this.category = res;
        this.categoryForm.patchValue({
          name : this.category.name,
          icon : this.category.icon
        })
      }
    })

  }

  updateCat() {
    if(this.categoryForm.valid){
      let catUpdate : Category = this.categoryForm.value;
      this._categoryService.update(catUpdate, this.categoryId).subscribe({
        next : (res) => {
          this._router.navigateByUrl('/category');
        }
      })
    }
    else {
      this.categoryForm.markAllAsTouched();
    }
  }
}
