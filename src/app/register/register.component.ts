import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { Register } from '../models/register-model';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  //On oublie pas d'importer ReactiveFormsModule dans le module
  registerForm : FormGroup;

  constructor(private _fb : FormBuilder, private _authService : AuthService,
               private _router : Router) { 
    this.registerForm = _fb.group({
      pseudo : [null, [Validators.required]],
      email : [null, [Validators.required, Validators.email]],
      firstname : [null, [Validators.required]],
      lastname : [null, [Validators.required]],
      password : [null, [Validators.required, Validators.minLength(8), Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W|_]).+$/)]],
      confirmPassword : [null, [Validators.required]]
    }, { validators : [ this.passwordValidator ] })
  }

  ngOnInit(): void {
  }

  register() : void {
    if(this.registerForm.valid){
      //Si formulaire valide
      console.log(this.registerForm.value);
      let registerValue : Register = this.registerForm.value;
      this._authService.register(registerValue)
      // .subscribe({
      //   next : (res) => {
      //     //Si on a un res, on le stock en session
      //     localStorage.setItem('token', res.token);
      //     //Et redirection vers une page
      //     this._router.navigateByUrl('');
      //   },
      //   error : () => {},
      //   complete : () => {}
      // })
      
    }
    else {
      this.registerForm.markAllAsTouched();
    }

  }


  //Validateur custom pour vérifier les 2 mdp
  passwordValidator(group : FormGroup) {
    //On vérifie en premier si les deux input contiennent une valeur
    if(group.get('password')?.value != "" && group.get('confirmPassword')?.value != ""){
      //On vérifie si les deux mdp sont différents pour renvoyer une erreur, sinon on renvoie null
      if(group.get('password')?.value != group.get('confirmPassword')?.value){
        return { notsame : true }
      }
      return null;
    }
    return null;    
  }

}
