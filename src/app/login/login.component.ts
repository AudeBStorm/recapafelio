import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Login } from '../models/login-model';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup;

  constructor(private _fb : FormBuilder, private _authService : AuthService,
               private _router : Router) {
    this.loginForm = this._fb.group({
      credential : [null, [Validators.required]],
      password : [null, [Validators.required]]
    })
   }

  ngOnInit(): void {
  }

  login() : void {
    if(this.loginForm.valid){
      let loginValue : Login = this.loginForm.value;
      this._authService.login(loginValue)
      // .subscribe({
      //   next : (res) => {
      //     localStorage.setItem('token', res.token);
      //     this._router.navigateByUrl("/");
      //   },
      //   error : () => {},
      //   complete : () => {}
      // })
    }
    else {
      this.loginForm.markAllAsTouched();
    }
  }

}
